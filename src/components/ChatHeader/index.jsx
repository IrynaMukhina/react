import React, {Component} from 'react';
import { Menu } from 'semantic-ui-react'

import styles from './styles.module.scss';

class ChatHeader extends Component {
  render() {
    return (
      <div className={styles.chat_header__wrapper}>
      <Menu secondary>
        <Menu.Item
          className={styles.chat_header_item}
        >
          My Chat
        </Menu.Item>
        <Menu.Item
          className={styles.chat_header_item}
        >
          23 Participants
        </Menu.Item>
        <Menu.Item
          className={styles.chat_header_item}
        >
          54 Messages
        </Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item
            className={styles.chat_header_item}
          >
            Last Message at 14:28
          </Menu.Item>
        </Menu.Menu>
      </Menu>
      </div>
    );
  }
}

ChatHeader.propTypes = {

};

export default ChatHeader;
