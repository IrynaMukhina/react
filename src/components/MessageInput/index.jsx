import React, {Component} from 'react';

import styles from './styles.module.scss';
import { Input } from 'semantic-ui-react'

class MessageInput extends Component {
  
  render() {
    return (
      <div className={styles.message_input__wrapper}>
         <Input fluid action={{content: 'Send', color: 'red'}} placeholder='Your Message' />
      </div>
    );
  }
}


MessageInput.propTypes = {

};

export default MessageInput;