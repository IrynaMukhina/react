import React, {Component} from 'react';
import { Comment } from 'semantic-ui-react'
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

class Message extends Component {
  render() {
    return (
      <Comment className={this.props.message.user === 'Wendy' ? styles.message_my : styles.message_others}>
        {this.props.message.user !== 'Wendy' && <Comment.Avatar src={this.props.message.avatar} />}
        <Comment.Content className={styles.message}>
          <Comment.Author as='a'>{this.props.message.user}</Comment.Author>
          <Comment.Metadata>
            <div>{this.props.message.createdAt}</div>
          </Comment.Metadata>
          <Comment.Text>{this.props.message.text}</Comment.Text>
          <Comment.Actions>
            <Comment.Action>Like</Comment.Action>
          </Comment.Actions>
        </Comment.Content>
      </Comment>
    );
  }
}

Message.propTypes = {
  message: PropTypes.object,
};

export default Message;