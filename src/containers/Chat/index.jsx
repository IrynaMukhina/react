import React, {Component} from 'react';
import { Grid, Dimmer, Loader } from 'semantic-ui-react';
import MessageList from '../../components/MessageList';
import ChatHeader from '../../components/ChatHeader';
import MessageInput from '../../components/MessageInput'
import axios from 'axios';

import styles from './styles.module.scss';

const CHAT_DATA_URL = 'https://api.jsonbin.io/b/5f15f73f9180616628457f77';

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      chatData: [],
    };
  }

  render() {
    return (
      <Grid className={styles.chat__wrapper}>
        <Dimmer active={this.state.isLoading}>
          <Loader />
        </Dimmer>
        <ChatHeader></ChatHeader>
        <MessageList messages={this.state.chatData}></MessageList>
        <MessageInput></MessageInput>
      </Grid>
    );
  }

  componentDidMount() {
    this.getData();
  }

  async getData(){  
    this.setState({isLoading: true});

    this.setState({chatData: (await axios.get(CHAT_DATA_URL)).data});

    this.setState({isLoading: false});
  };
}

export default Chat;