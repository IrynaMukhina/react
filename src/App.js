import React from 'react';
import Chat from './containers/Chat/index';
import Header from './components/Header/index';
import './App.css';
import 'semantic-ui-css/semantic.min.css'

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Chat></Chat>
    </div>
  );
}

export default App;
