import React, {Component} from 'react';
import Message from '../Message/index';
import { Comment } from 'semantic-ui-react'

import styles from './styles.module.scss';
import PropTypes from 'prop-types';

class MessageList extends Component {
  render() {
    return (
      <Comment.Group className={styles.message_list__wrapper}>
        {this.props.messages.map(
          message => <Message message={message}></Message>
        )}
      </Comment.Group>
    );
  }
}

MessageList.propTypes = {
  messages: PropTypes.array,
};

export default MessageList;