import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Header as HeaderUI, Image } from 'semantic-ui-react';
import logo from '../../assets/rocket-chat.svg';


import styles from './styles.module.scss';

class Header extends Component {
  render() {
    return (
      <div>
        <HeaderUI className={styles.header__wrapper}>
          <Image src={logo} className={styles.header__logo}/>
          <h2 className={styles.header__title}>Chat</h2>
        </HeaderUI>
      </div>
    );
  }
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
